import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatingPageComponent} from "./creating-page.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {CreatingPageRoutingModule} from "./creating-page-routing.module";
import {TagsListModule} from "../../common/components/tags-list/tags-list.module";
import {CustomTagsInputModule} from "../../common/components/custom-tags-input/custom-tags-input.module";
import {DropImageModule} from "./image-input-area/drop-image/drop-image.module";
import {SpinnerComponentModule} from "../../common/components/spinner/spinner.module";

@NgModule({
  declarations: [CreatingPageComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    RouterModule,
    CreatingPageRoutingModule,
    TagsListModule,
    CustomTagsInputModule,
    DropImageModule,
    SpinnerComponentModule
  ],
  exports: [CreatingPageComponent],
  providers: [FormBuilder]
})
export class CreatingPageModule {

}
