import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {previewSelectors} from 'src/app/core/selectors/preview.selectors';
import {PREVIEW_ACTION} from 'src/app/core/actions/preview.actions';
import {FirebaseService} from 'src/app/core/services/back-end/firebase';
import {ArticleContent} from "../../../articles.interfaces";

@Component({
  selector: 'app-preview-popup',
  templateUrl: './preview-popup.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./preview-popup.component.scss']
})
export class PreviewPopupComponent implements OnInit {
  data!: ArticleContent;
  sendLoading: boolean = false;
  spinnerStyle = {
    height: 'calc(100vh - 10px)'
  };
  constructor(private store: Store,
              private service: FirebaseService) {
  }

  ngOnInit(): void {
    this.store.select(previewSelectors.previewData).subscribe((articleData) => this.data = articleData)
  }

  saveData() {
    this.service.sendData(this.data);
    this.sendLoading = true;
    this.store.dispatch(PREVIEW_ACTION.UPLOAD_PREVIEW({
      previewData: {
        title: '',
        contents: [],
        preview: '',
        tags: [],
        author: '',
        subTitles: [],
        img: '',
        id: ''
      }
    }))
  }
}
