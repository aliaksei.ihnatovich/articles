import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PreviewPopupComponent} from "./preview-popup.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {SpinnerComponentModule} from "../../../common/components/spinner/spinner.module";
import {RouterModule} from "@angular/router";
import {ArticleModule} from "../../../common/components/article/article.module";



@NgModule({
  declarations: [PreviewPopupComponent],
    imports: [
        CommonModule,
        SpinnerComponentModule,
        RouterModule,
        ArticleModule
    ],
  exports: [PreviewPopupComponent]
})
export class PreviewPopupModule { }
