import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropImageComponent } from './drop-image.component';
import {DropImageDirective} from "../drop-inage.directive";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    DropImageComponent,
    DropImageDirective
  ],
  exports: [
    DropImageComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class DropImageModule { }
