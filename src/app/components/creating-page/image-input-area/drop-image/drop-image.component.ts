import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-drop-image',
  templateUrl: './drop-image.component.html',
  styleUrls: ['./drop-image.component.scss']
})
export class DropImageComponent implements OnInit {

  constructor() {
  }
  @Output() imageBase64 = new EventEmitter<string | ArrayBuffer>();
  files: any[] = [];
  @Input()  inputImageUrl;
  /**
   * on file drop handlerS
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
    this.takeImage($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files: any) {
    this.prepareFilesList(files.files[0]);
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    this.files.splice(index, 1);
    this.inputImageUrl = null;
  }

  takeImage(event: Event) {
    let file = ((<HTMLInputElement>event?.target)?.files) || event;
    const reader = new FileReader();
    reader.onloadend = (event) => {
      this.inputImageUrl = event.target?.result;
      this.imageBase64.emit(event.target?.result)
    }

    if (file) {
      reader.readAsDataURL(file[0] || file);
    }
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
      this.files.push(files);
  }
  ngOnInit(): void {
    if(!!this.inputImageUrl) {
      this.files.push(this.inputImageUrl)
    }
  }

}
