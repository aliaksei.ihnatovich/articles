import {Component, Input} from '@angular/core';
import {AuthService} from '../../core/services/authenticators/authentication.service';
import {AuthCheckGuard} from '../../core/guards/auth-check.guard';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent {
  constructor(public auth: AuthCheckGuard,
              public authCheck: AuthService,
              private http: HttpClient) {
    authCheck.userInfo$.subscribe((user) => this.user = user?.userLog)
  }
  user!: object | null | undefined
  @Input() isChecked = false;

  loginGoogle() {
    this.authCheck.googleLogin();
  }

  getData() { //testing get
   return this.http.get('https://jsonplaceholder.typicode.com/todos');
  }
}
