import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FooterComponentModule} from "../../common/components/footer/footer-component.module";
import {SpinnerComponentModule} from "../../common/components/spinner/spinner.module";
import {EmptyStateModule} from "../../common/components/empty-state/empty-state.module";
import {MainPageComponent} from "./main-page.component";
import {ArticlePrevComponentModule} from "./article-prev/article-prev-component.module";
import {FormsModule} from "@angular/forms";
import {SearchFilterPipe} from "./search-filter.pipe";
import {TagsListModule} from "../../common/components/tags-list/tags-list.module";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [MainPageComponent, SearchFilterPipe],
  imports: [
    CommonModule,
    MatPaginatorModule,
    FooterComponentModule,
    SpinnerComponentModule,
    EmptyStateModule,
    ArticlePrevComponentModule,
    FormsModule,
    TagsListModule,
    MatIconModule
  ],
  exports: [MainPageComponent],
  providers: [SearchFilterPipe]
})
export class MainPageModule { }
