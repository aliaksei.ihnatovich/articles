import {Component, OnInit} from '@angular/core';
import {FirebaseService} from '../../core/services/back-end/firebase';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Article} from "../../articles.interfaces";
import {TagsService} from "../../common/tags.service";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})

export class MainPageComponent implements OnInit {
  searchDebouncer$: Subject<string> = new Subject();
  searchTagsDebouncer$: Subject<string[]> = new Subject();
  articlesData: Article;
  articlesList: Article;
  tagsList: string[];
  spinnerStyle = {
    height: 'calc(100vh - 10px)'
  };
  constructor(public service: FirebaseService,
              private tagsService: TagsService) {}

  inputValue: string = '';
  activeTags: string[] = [];

  ngOnInit() {
    this.tagsService.tagsList.subscribe(tags => {
      this.tagsList = tags;
    });
    this.service.getArticlesList()
    this.service.listOfArticles.subscribe((articles: Article) => {
      this.articlesData = this.articlesList = articles;
    });
    this.searchDebouncer$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
    ).subscribe(() => {
      this.articlesList = this.service.filterArticleList(this.inputValue, this.activeTags, this.articlesData)
    });
    this.searchTagsDebouncer$.pipe(
    ).subscribe((tags) => {
      this.articlesList = this.service.filterArticleList(this.inputValue, this.activeTags, this.articlesData)
    });
  }

  findTag(tags: string[]) {
    this.activeTags = tags;
    this.searchTagsDebouncer$.next(tags);
  }

  search() {
    this.searchDebouncer$.next(this.inputValue);
  }
}
