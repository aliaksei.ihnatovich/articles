import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ArticlePageComponent} from "./article-page.component";
import {SpinnerComponentModule} from "../../common/components/spinner/spinner.module";
import {FooterComponentModule} from "../../common/components/footer/footer-component.module";
import {RouterModule} from "@angular/router";
import {RecommendationBlockModule} from "../../common/components/recomendation-block/recommendation-block.module";
import {ArticlePageRoutingModule} from "./article-page-routing.module";
import {ArticleModule} from "../../common/components/article/article.module";

@NgModule({
  declarations: [ArticlePageComponent],
  imports: [
    CommonModule,
    RecommendationBlockModule,
    SpinnerComponentModule,
    RouterModule,
    RecommendationBlockModule,
    FooterComponentModule,
    ArticlePageRoutingModule,
    ArticleModule
  ],
  exports: [ArticlePageComponent]
})
export class ArticlePageModule { }
