import { Injectable} from '@angular/core';
import { AuthCheckGuard } from '../guards/auth-check.guard';
import {THEME} from "./theme.constants";

@Injectable({ providedIn: 'root' })

export class ThemeChanger {
  themeInformation = {
    currentTheme:  localStorage.getItem('theme') || 'light',
    nextTheme:  !localStorage.getItem('theme') || localStorage.getItem('theme') === 'light' ? 'dark' : 'light',
    isThemeChecked: !( localStorage.getItem('theme') === 'light') && !!localStorage.getItem('theme')
  }

  constructor(public auth: AuthCheckGuard) {
    if(localStorage.getItem('theme'))
      this.toggleTheme(this.themeInformation.isThemeChecked)
  }

  updateThemeParameters(newTheme: string, nextTheme: string) {
    this.themeInformation.currentTheme = newTheme;
    this.themeInformation.nextTheme = nextTheme
    localStorage.setItem('theme', newTheme)
  }

  toggleTheme(isChecked: boolean) {
    isChecked ? this.updateThemeParameters('dark', 'light')
      : this.updateThemeParameters('light', 'dark');

    const theme = THEME[this.themeInformation.currentTheme];
    for (let color in theme) {
      document.documentElement.style.setProperty(color, theme[color]);
    }
  }
}
