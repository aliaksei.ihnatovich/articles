export const THEME = {
  light: {
    '--bg-color': '#FFFFFF',
    '--bg-color-secondary': '#c9c9c9',
    '--font-primary-color': '#1F1F1F',
    '--font-secondary-color': '#64676A',
    '--button-font-color': '#FFFFFF',
    '--grey-color-1': '#1F1F1F',
    '--grey-color-2': '#64676A',
    '--grey-color-3': '#E5E5E5',
    '--blue-color': '#2F80ED',
    '--light-blue-color': '#3c87d3',
    '--orange-color-1': '#F0AB00'
  },
  dark: {
    '--bg-color': '#333333',
    '--bg-color-secondary': '#000000',
    '--font-primary-color': '#dcdcdc',
    '--font-secondary-color': '#bbbbbb',
    '--button-font-color': '#e3e3e3',
    '--grey-color-1': '#1F1F1F',
    '--grey-color-2': '#64676A',
    '--grey-color-3': '#bbbbbb',
    '--blue-color': '#30537a',
    '--light-blue-color': '#3c87d3',
    '--orange-color-1': '#ab7b00'
  }
}
