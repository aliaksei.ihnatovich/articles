import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CanDeactivate, Router, UrlTree} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {CreatingPageComponent} from '../../components/creating-page/creating-page.component';
import {previewSelectors} from '../selectors/preview.selectors';
import {ArticleContent} from "../../articles.interfaces";
import {ConfirmDialogComponent} from "../../common/components/confirm-dialog/confirm-dialog.component";
import {AuthService, UserInfo} from "./authenticators/authentication.service";

@Injectable({providedIn: 'root'})

export class CanDeactivateForm implements CanDeactivate<CreatingPageComponent> {

  articleData!: ArticleContent;
  userInfo!: UserInfo;

  constructor(private dialog: MatDialog,
              private store: Store,
              private auth: AuthService) {
    this.auth.userInfo$.subscribe((user) => this.userInfo = {
      userName: user?.userName,
      imgUrl: user?.imgUrl,
      userLog: user?.userLog
    })
    this.store.select(previewSelectors.previewData).subscribe((articleData) => this.articleData = articleData)
  }

  canDeactivate(
    component: CreatingPageComponent
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if ((this?.auth?.userLogin && component?.form?.touched && !!this?.articleData?.img)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent)

      return dialogRef.afterClosed()
    }
    return true;
  }
}
