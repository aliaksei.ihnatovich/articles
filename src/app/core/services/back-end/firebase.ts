import {Injectable} from '@angular/core';
import {AngularFirestore, DocumentData, DocumentReference} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import firebase from 'firebase';
import {BehaviorSubject} from 'rxjs';
import {Article, ArticleContent} from "../../../articles.interfaces";
import {SearchFilterPipe} from "../../../components/main-page/search-filter.pipe";

@Injectable({
  providedIn: 'root'
})

export class FirebaseService {
  constructor(public fireservices: AngularFirestore,
              public router: Router,
              public searchFilter: SearchFilterPipe) {
  }

  db = this.fireservices.firestore;
  articleInitialValue = {
    id: '',
    subTitles: [''],
    title: '',
    img: '',
    tags: [''],
    author: '',
    preview: '',
    contents: ['']
  };
  listOfArticles = new BehaviorSubject<Article>({isLoading: true, articlesList: [this.articleInitialValue]});
  fireStorage = firebase.storage().ref('Images');

  sendData(data: ArticleContent) {
    const image = data.img.toString();
    const base64Image = image.substring((image.indexOf('base64') + 7));
    const randomID = [...Array(30)].map(() => Math.random().toString(36)[2]).join('');
    const thisRef = this.fireStorage.child(`photo-${randomID}`); // add current time to unique name
    thisRef.putString(base64Image, 'base64').then(res => {
        thisRef.getDownloadURL()
          .then((url) => {
            let imgUrl = url;
            this.db.collection("articles").add({
              id: ``,
              img: url,
              title: data.title,
              subTitles: data.subTitles,
              contents: data.contents,
              preview: data.preview,
              tags: data.tags,
              author: data.author
            })
              .then((ref: DocumentReference) => {
                const docId = ref.id;
                this.db.collection("articles").doc(docId).update({id: docId}).then(() => {
                  this.router.navigate([`article/${docId}`])
                })
                // relocate to new article
              })
          })
      }
    )
  }

  getArticlesList() {
    this.fireservices.collection('articles').valueChanges().subscribe((articles: ArticleContent[]) => {
      this.listOfArticles.next({isLoading: false, articlesList: articles});
    })
  }

  filterArticleList(input: string, tags: string[], articles: Article) {
    return {
      isLoading: false,
      articlesList: this.searchFilter.transform(articles.articlesList, tags, input)
    };
  }
}
