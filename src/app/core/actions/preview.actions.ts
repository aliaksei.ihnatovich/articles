import {createAction, props} from "@ngrx/store";
import {ArticleContent} from "../../articles.interfaces";

export namespace PREVIEW_ACTION {
  export const UPLOAD_PREVIEW = createAction('UPLOAD_PREVIEW',
    props<{ previewData: ArticleContent }>()
  )
  export const DELETE_PREVIEW = createAction('DELETE_PREVIEW')
  export const CHANGE_THEME = createAction('CHANGE_THEME',
    props<{ theme: string }>())
  export const ADD_TAGS = createAction('CHANGE_THEME',
    props<{ selectedTags: string[] }>())
}

