import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ArticlePageComponent} from './components/article-page/article-page.component';
import {AuthPageComponent} from './components/auth-page/auth-page.component';
import {CreatingPageComponent} from './components/creating-page/creating-page.component';
import {ErrorPageComponent} from './components/error-page/error-page.component';
import {MainPageComponent} from './components/main-page/main-page.component';
import {PreloadAllModules} from '@angular/router';
import {AuthCheckGuard} from './core/guards/auth-check.guard';
import {CanDeactivateForm} from './core/services/canDeacrivate.service';
import {UserResolver} from './core/services/resolver';
import {PreviewPopupComponent} from './components/creating-page/preview-popup/preview-popup.component';
import {APP_BASE_HREF, HashLocationStrategy, LocationStrategy} from "@angular/common";

const routes: Routes = [
  {
    path: 'creating',
    canLoad: [AuthCheckGuard],
    canDeactivate: [CanDeactivateForm],
    loadChildren: () => import('./components/creating-page/creating-page.module').then(m => m.CreatingPageModule),
    resolve: {users: UserResolver}
  },
  {path: 'autorization', component: AuthPageComponent},
  {path: 'article/:id', loadChildren: () => import('./components/article-page/article-page.module').then(m => m.ArticlePageModule)},
  {path: 'error', pathMatch: 'full', component: ErrorPageComponent},
  {
    path: 'creating/preview',
    component: PreviewPopupComponent,
    canLoad: [AuthCheckGuard],
    canDeactivate: [CanDeactivateForm]
  },
  {path: '', pathMatch: 'full', component: MainPageComponent},
  {path: '**', redirectTo: 'error'},
];

RouterModule.forRoot(
  routes,
  {
    preloadingStrategy: PreloadAllModules
  }
)

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '' },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ]
})


export class AppRoutingModule {
}
