import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Article} from "../articles.interfaces";
import {FirebaseService} from "../core/services/back-end/firebase";
import {TagListFilterPipe} from "./components/tags-list/tag-list-filter.pipe";

@Injectable({providedIn: 'root'})

export class TagsService {
  public tagsList = new BehaviorSubject<string[]>([]);
  public ActiveTagsList$ = new BehaviorSubject<string[]>([]);

  constructor(public fireService: FirebaseService,
              private tagsFilterPipe: TagListFilterPipe) {
    this.fireService.getArticlesList();

    this.fireService.listOfArticles.subscribe((articles: Article) => {
      this.tagsList.next(this.tagsFilterPipe.transform(articles));
    });
  }

  addTag(tags: string[]) {
    this.ActiveTagsList$.next(tags);
  }

  refreshTags() {
    this.ActiveTagsList$.next([])
  }
}

