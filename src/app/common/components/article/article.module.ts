import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article.component';
import {SpinnerComponentModule} from "../spinner/spinner.module";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    ArticleComponent
  ],
  imports: [
    CommonModule,
    SpinnerComponentModule,
    RouterModule
  ],
  exports: [ArticleComponent]
})
export class ArticleModule { }
