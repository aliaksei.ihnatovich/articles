import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ArticleContent} from "../../../articles.interfaces";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnChanges {

  @Input() articleData: ArticleContent;
  backgroundImageUrl: object;
  spinnerStyle = {
    height: 'calc(100vh - 10px)'
  };
  constructor() {}

  ngOnChanges() {
    this.backgroundImageUrl = {
      'background-image': `url(${this?.articleData?.img})`
    };
  }
}
