import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {TagsService} from "../../tags.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.scss']
})
export class TagsListComponent implements OnDestroy {

  constructor(private tagsService: TagsService) {}

  @Output() addTagEvent = new EventEmitter<string[]>();
  @Input() selectedTags: string[];
  @Input() tagsList: string[];
  defaultDisplayingTagsCount = 10;
  @Input() displayingTagsCount = this.defaultDisplayingTagsCount;
  collectTag(tag: string) {
    if (this.selectedTags.indexOf(tag) !== -1) {
      this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);

    } else {
      this.selectedTags.push(tag)
    }
    this.tagsService.addTag(this.selectedTags);
    this.addTagEvent.emit(this.selectedTags);
  }

  hideTags() {
    if(this.displayingTagsCount !== this.defaultDisplayingTagsCount) {
      this.displayingTagsCount = this.defaultDisplayingTagsCount;
    }
  }

  showMoreTags() {
    this.displayingTagsCount+=5;
  }

  ngOnDestroy() {
    this.tagsService.refreshTags();
  }
}
