import {Pipe, PipeTransform} from '@angular/core';
import {Article, ArticleContent} from "../../../articles.interfaces";
import {BehaviorSubject} from "rxjs";

@Pipe({
  name: 'tagListFilter'
})
export class TagListFilterPipe implements PipeTransform {

  transform(article: Article): string[] {
    let tagsList = [],
      sortedTags = [];
    let tagCounter = {};
    article.articlesList.forEach(articleContent => articleContent.tags.forEach((articleTag) => {
      if (!article.isLoading) {
        if (!tagsList.some(existingTag => articleTag === existingTag)) {
          tagsList.push(articleTag);
        }
        (tagCounter[articleTag] >= 0 ? tagCounter[articleTag]++ : tagCounter[articleTag] = 0);
      }
    }));

    Object.keys(tagCounter).forEach(tagName => sortedTags.push(
        {
          tagName: tagName,
          tagCounter: tagCounter[tagName]
        }
      )
    );
    return sortedTags
      .sort((a, b) => {
        return a.tagCounter - b.tagCounter;
      })
      .reverse()
      .map(value => value.tagName)
  }
}

