import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagsListComponent } from './tags-list.component';
import {SpinnerComponentModule} from "../spinner/spinner.module";
import { TagListFilterPipe } from './tag-list-filter.pipe';



@NgModule({
  declarations: [
    TagsListComponent,
    TagListFilterPipe
  ],
  exports: [
    TagsListComponent
  ],
    imports: [
        CommonModule,
        SpinnerComponentModule
    ],
  providers: [TagListFilterPipe]
})
export class TagsListModule { }
