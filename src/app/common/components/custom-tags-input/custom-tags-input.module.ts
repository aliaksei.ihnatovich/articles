import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomTagsInputComponent } from './custom-tags-input.component';
import {MatChipsModule} from "@angular/material/chips";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";



@NgModule({
  declarations: [
    CustomTagsInputComponent
  ],
  exports: [
    CustomTagsInputComponent
  ],
  imports: [
    CommonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule
  ]
})
export class CustomTagsInputModule { }
