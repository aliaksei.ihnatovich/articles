import {Component, Input, OnInit} from '@angular/core';
import {MatChipInputEvent} from "@angular/material/chips";
import {TagsService} from "../../tags.service";
import {COMMA, ENTER} from "@angular/cdk/keycodes";


@Component({
  selector: 'app-custom-tags-input',
  templateUrl: './custom-tags-input.component.html',
  styleUrls: ['./custom-tags-input.component.scss']
})
export class CustomTagsInputComponent implements OnInit {

  constructor(private tagsService: TagsService) {}
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  selectable = true;
  removable = true;
  addOnBlur = true;
  @Input() selectedTags: string[];
  ngOnInit(): void {
    this.tagsService.ActiveTagsList$.subscribe(tags => this.selectedTags = tags)
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value && this.selectedTags.indexOf(value) === -1) {
      this.selectedTags.push(value)
      this.tagsService.addTag(this.selectedTags);
    }
    event.chipInput!.clear();
  }

  remove(tag: string): void {
    const index = this.selectedTags.indexOf(tag);
    if (index >= 0) {
      this.selectedTags.splice(index, 1);
    }
  }
}
