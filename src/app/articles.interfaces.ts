export interface ArticleContent {
  id: string;
  author: string;
  contents: string[];
  img: string | ArrayBuffer;
  preview: string;
  subTitles: string[];
  tags: string[];
  title: string;
}

export interface Article {
  isLoading: boolean;
  articlesList: ArticleContent[];
}
