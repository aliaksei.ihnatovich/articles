// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA7UTQwaEeOuusRL1suuE5haO6aiZJfZQ4",
    authDomain: "autorize-2.firebaseapp.com",
    projectId: "autorize-2",
    storageBucket: "autorize-2.appspot.com",
    messagingSenderId: "742417600313",
    appId: "1:742417600313:web:5f49c12b3181c5798fa2e8",
    measurementId: "G-B7H3ZRBHW7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
